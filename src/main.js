import Vue from 'vue';
import VueAnalytics from 'vue-analytics';
import App from './App.vue';
import router from './router';

Vue.config.productionTip = false;
// Configuration VueAnalytics
Vue.use(VueAnalytics, {
  id: 'UA-213776380-1',
  router,
});
new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
